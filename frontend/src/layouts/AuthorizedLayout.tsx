import {Container} from "@chakra-ui/react";

function AuthorizedLayout({children} : {children: React.ReactElement}) {
  return (
    <>
      <Container>
        {children}
      </Container>
    </>
  );
}

export {AuthorizedLayout}