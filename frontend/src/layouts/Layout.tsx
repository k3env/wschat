import {GuestLayout} from "@layouts/GuestLayout.tsx";
import {AuthorizedLayout} from "@layouts/AuthorizedLayout.tsx";
import {useLocalStorage} from "@/hooks/useLocalStorage.ts";
import {HeaderComponent} from "@/components";
import React from "react";

export function Layout({children} : {children: React.ReactElement}) {
	const token = useLocalStorage("token", null)
	const UsedLayout = token == null ? GuestLayout : AuthorizedLayout
	return (
		<>
			<HeaderComponent />
			<UsedLayout>
				{children}
			</UsedLayout>
		</>

	)
	// if (token == null) {
	// 	return GuestLayout({children})
	// }
	// return AuthorizedLayout({children})
}