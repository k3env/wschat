import {Container, Flex} from "@chakra-ui/react";

function GuestLayout({children} : {children: React.ReactElement}) {
	return (
		<>
			<Flex h="80dvh" justifyContent="center" alignItems="center">
				<Container>
					{children}
				</Container>
			</Flex>
		</>
	);
}

export {GuestLayout}