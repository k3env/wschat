import { AuthFormData } from "./AuthFormData.ts";
import {Message} from "./Message.ts";
import {ResponseLogin, ResponseRegister, ResponseError} from "./APIResponses.ts";
import {JWT, UserInfo} from "./JWT.ts";

export type {Message, AuthFormData, ResponseLogin, ResponseRegister, ResponseError, UserInfo, JWT}