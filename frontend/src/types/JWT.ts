interface JWT {
	exp: number
	sub: string
	sid: string
}

interface UserInfo {
	username: string,
	expirity: Date
	session: string
}

export type {JWT, UserInfo}