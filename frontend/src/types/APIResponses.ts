interface ResponseError {
	status: "error",
	error: string
}

interface ResponseRegister {
	status: "ok",
	data: string
}

interface ResponseLogin {
	status: "ok",
	data: string
}

export type {ResponseError, ResponseLogin, ResponseRegister}