interface Message {
    from: string
    text: string
    to: string
    timestamp: number
}
export type {Message}