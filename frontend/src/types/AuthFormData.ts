interface AuthFormData {
	username: string,
	password: string
}

export type {AuthFormData}