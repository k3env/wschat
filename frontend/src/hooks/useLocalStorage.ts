import React from "react";

const useLocalStorage = <T>(storageKey: string, fallbackState: T) => {
	const data = localStorage.getItem(storageKey)
	const [value, setValue] = React.useState(
		data == null ? fallbackState : data
	);

	React.useEffect(() => {
		if (data != null) localStorage.setItem(storageKey, data);
	}, [value, storageKey]);

	return [value, setValue];
};

export {useLocalStorage}