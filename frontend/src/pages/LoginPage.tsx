import {
	Button,
	Card,
	CardBody, CardFooter,
	CardHeader,
	Container,
	Flex,
	FormControl,
	FormLabel, Heading, HStack,
	Input, InputGroup, InputRightElement,
	useBoolean, useToast, VStack
} from "@chakra-ui/react";
import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {HideIcon, ShowIcon} from "@/components";
import {AuthFormData, ResponseError, ResponseLogin} from "@/types";
import {request} from "@/helpers/apiRequest.ts";

function LoginPage() {
	const [show, setShow] = useBoolean(false)
	const [username, setUsername] = useState("")
	const [password, setPassword] = useState("")
	const nav = useNavigate()
	const toast = useToast()

	const handleLoginClick = (/*e: MouseEvent<HTMLButtonElement>*/) => {
		// e.preventDefault()
		const fd : AuthFormData = {username, password}
		request(`${import.meta.env.VITE_BACKEND_URL}/api/login`, {method: "POST", body: JSON.stringify(fd)}, handleSuccess, handleError)
	}

	const handleSuccess = (r: string) => {
		const res: ResponseLogin = JSON.parse(r)
		localStorage.setItem("token", res.data)
		nav("/", {replace: true, relative: "path"})
	}

	const handleError = (e: string) => {
		const data: ResponseError = JSON.parse(e)
		toast({
			status: data.status,
			title: "Something goes wrong",
			description: data.error
		})
	}

	return (
		<Flex h="80dvh" justifyContent="center" alignItems="center">
			<Container>
				<Card>
					<CardHeader>
						<Heading>Login</Heading>
					</CardHeader>
					<CardBody>
						<VStack gap={2}>
							<FormControl>
								<FormLabel>Username</FormLabel>
								<Input onChange={(e) => setUsername(e.target.value)} name="username" value={username}/>
							</FormControl>
							<FormControl>
								<FormLabel>Password</FormLabel>
								<InputGroup>
									<Input type={show ? 'text' : 'password'} onChange={(e) => setPassword(e.target.value)} name="password" value={password}/>
									<InputRightElement>
										<Button size="sm" onClick={setShow.toggle}>{show ? <HideIcon /> : <ShowIcon />}</Button>
									</InputRightElement>
								</InputGroup>
							</FormControl>
						</VStack>
					</CardBody>
					<CardFooter>
						<HStack gap={2}>
							<Button type="submit" onClick={handleLoginClick}>Login</Button>
							<Button as={Link} to={"/register"}>Register</Button>
							<Button as={Link} to={"/forgot"}>Forgot password</Button>
						</HStack>
					</CardFooter>
				</Card>
			</Container>
		</Flex>
	)
}

export {LoginPage}