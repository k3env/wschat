import {Button, FormControl, FormLabel, HStack, Input, Text, useBoolean, VStack} from "@chakra-ui/react";
import {MessageComponent} from "@/components";
import {useCallback, useEffect, useRef, useState} from "react";
import {Message} from "@/types";
import {SendIcon} from "@/components";
import {getUserInfo} from "@/helpers/parseToken.ts";
import {Navigate} from "react-router-dom";

function ChatPage() {
	const [chatLog, setChatLog] = useState<Message[]>([])
	const [isPaused, changePause] = useBoolean(false)
	const [msg, setMsg] = useState<string>("")
	const [user, setUser] = useState<string>("")
	const [status, setStatus] = useState(0)
	const ws = useRef<WebSocket|null>(null)

	const statuses = [
		'Connecting',
		'Connected',
		'Closing',
		'Closed',
	]

	const token = getUserInfo()
	if (token == null) {
		return <Navigate to="/login" />
	}

	// useEffect(() => {
	// 	setUser(token.username)
	// }, [token]);


	useEffect(() => {
		if (!isPaused) {
			ws.current = new WebSocket(`${import.meta.env.VITE_WS_URL}/ws/${token.session}`)
			ws.current.onclose = () => setStatus(WebSocket.CLOSED)
			ws.current.onopen = () => setStatus(WebSocket.OPEN)
			listening()
		}

		return () => ws.current?.close()
	}, [isPaused]);

	const listening = useCallback(() => {
		if (!ws.current) return;

		ws.current.onmessage = e => {
			if (isPaused) return;
			console.log(e.data)
			const msg = JSON.parse(e.data);
			if (msg !== null) {
				setChatLog((prev) => prev.concat(msg));
			}
		}
	}, [isPaused])

	let connectionStatus = statuses[status]

	useEffect(() => {
		if (ws.current == null) return
		setStatus(ws.current.readyState)
	}, [ws.current?.readyState]);



	const onSendClick = () => {
		if (ws.current == null) return
		const m: Message = {
			text: msg,
			from: token.username,
			to: user,
			timestamp: Date.now()
		}

		ws.current.send(JSON.stringify(m))
		setMsg("")
	}
	return (
		<>
			<HStack justifyContent={"space-between"}>
				<Text>Status: {connectionStatus}</Text>
				<Button onClick={changePause.toggle}>{isPaused ? "Connect" : "Disconnect"}</Button>
			</HStack>
			<VStack align={"stretch"}>
				<FormControl>
					<FormLabel>Send to</FormLabel>
					<Input onChange={e => setUser(e.target.value)} value={user} />
				</FormControl>
				<HStack>
					<Input onChange={e => setMsg(e.target.value)} value={msg}></Input>
					<Button onClick={onSendClick}>
						<SendIcon />
					</Button>
				</HStack>
			</VStack>
			<VStack align={"start"}>
				{chatLog.map((l, k) => <MessageComponent key={k} msg={l}/>)}
			</VStack>
		</>
	)
}

export {ChatPage}