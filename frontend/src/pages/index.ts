import {LoginPage} from "@pages/LoginPage.tsx";
import {ChatPage} from "@pages/ChatPage.tsx";
import {RegisterPage} from "@pages/RegisterPage.tsx";
import {ForgotPasswordPage} from "@pages/ForgotPasswordPage"

export {LoginPage, ChatPage, RegisterPage, ForgotPasswordPage}