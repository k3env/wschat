import {
	Button,
	Card,
	CardBody, CardFooter,
	CardHeader,
	FormControl,
	FormLabel,
	Heading, HStack, Input, InputGroup, InputRightElement,
	useBoolean
} from "@chakra-ui/react";
import {useState} from "react";
import {Link, useNavigate} from "react-router-dom";
import {AuthFormData} from "@/types";
import {HideIcon, ShowIcon} from "@/components";
import {request} from "@/helpers/apiRequest.ts";

function RegisterPage() {
	const [show, setShow] = useBoolean(false)
	const [username, setUsername] = useState("")
	const [password, setPassword] = useState("")
	const nav = useNavigate()

	const handleRegisterClick = () => {
		const fd : AuthFormData = {username, password}
		request(`${import.meta.env.VITE_BACKEND_URL}/api/register`, {method: "POST", body: JSON.stringify(fd)}, () => nav("/login"), (e) => console.error(e))
	}

	return (
		<Card>
			<CardHeader>
				<Heading>Register account</Heading>
			</CardHeader>
			<CardBody>
				<FormControl>
					<FormLabel>Username</FormLabel>
					<Input onChange={(e) => setUsername(e.target.value)} id="username" value={username}/>
				</FormControl>
				<FormControl>
					<FormLabel>Password</FormLabel>
					<InputGroup>
						<Input type={show ? 'text' : 'password'} onChange={(e) => setPassword(e.target.value)} id="password" value={password}/>
						<InputRightElement>
							<Button size="sm" onClick={setShow.toggle}>{show ? <HideIcon /> : <ShowIcon />}</Button>
						</InputRightElement>
					</InputGroup>
				</FormControl>
			</CardBody>
			<CardFooter>
				<HStack gap={2}>
					<Button onClick={handleRegisterClick}>Register</Button>
					<Button as={Link} to={"/login"}>Login</Button>
				</HStack>
			</CardFooter>
		</Card>
	)
}

export {RegisterPage}