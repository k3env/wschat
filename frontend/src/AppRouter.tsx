import {createBrowserRouter, RouterProvider} from "react-router-dom";
import {layoutRoute} from "@/routes/routes.tsx";

export function AppRouter() {
	const router = createBrowserRouter([
		layoutRoute
	])

	return (
		// <Layout>
			<RouterProvider router={router} />
		// </Layout>
	)
}