import {Outlet, redirect, RouteObject} from "react-router-dom";
import {ChatPage, LoginPage, RegisterPage} from "@/pages";
import {getUserInfo} from "@/helpers/parseToken.ts";
import {Layout} from "@/layouts";



const routeRoot: RouteObject = {
	path: "/",
	element: <ChatPage />,
	loader: () => {
		const user = getUserInfo()
		if (user == null) {
			return redirect("/login")
		}
		return user
	}
}

const routeLogin: RouteObject = {
	path: "/login",
	element: <LoginPage />,
	action: async ({ params, request }) => {
		let formData = await request.formData();
		console.log(formData)
		console.log(params)
	}
}

const routeRegister: RouteObject = {
	path: "/register",
	element: <RegisterPage />,
	action: async (args) => {
		console.log(args)
		return null
	}
}

const layoutRoute: RouteObject = {
	element: (<Layout><Outlet /></Layout>),
	children: [routeRoot, routeLogin, routeRegister]
}

export {routeRegister, routeRoot, routeLogin, layoutRoute}