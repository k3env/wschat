import {Message} from "@/types";
import {Box, Text, VStack} from "@chakra-ui/react";

function timestampParser(ts: number): string {
	const formatter = Intl.DateTimeFormat("ru", {hour: "2-digit", minute: "2-digit"}).format
	return formatter(ts)
}

function calcUserColor(username: string): string {
	const colors = ["red", "orange", "yellow", "green", "teal", "blue", "cyan", "purple", "pink"]
	const enc =  new TextEncoder()
	const bytes = enc.encode(username)
	let sum = 0
	bytes.forEach(v => sum += v)
	return colors[sum % colors.length]
}

const MessageComponent = ({msg}: {msg: Message}) => {

	// const rn = Math.round(Math.random() * colors.length)

	return (
		<Box bg="gray.300" padding={1} width="75%" marginTop={3} rounded={4} color="gray.500">
			<VStack align="start" gap={0}>
				<Text as="b" color={`${calcUserColor(msg.from)}.500`}>{msg.from}:</Text>
				<Text>{msg.text}</Text>
				<Text fontSize='xs' alignSelf={"end"}>{timestampParser(msg.timestamp)}</Text>
			</VStack>
		</Box>
	)
}

export {MessageComponent}