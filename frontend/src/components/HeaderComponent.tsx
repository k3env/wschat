import {Text, HStack, Button, useColorMode} from "@chakra-ui/react";
import {SunIcon, MoonIcon} from "@components/Icons.tsx";
import {getUserInfo} from "@/helpers/parseToken.ts";
import {request} from "@/helpers/apiRequest.ts";
import {useNavigate} from "react-router-dom";

function HeaderComponent() {
	const {colorMode, toggleColorMode} = useColorMode()
	const token = getUserInfo()
	const nav = useNavigate()

	const handleLogout = () => {
		request(`${import.meta.env.VITE_BACKEND_URL}/api/logout`, {method: "POST"}, () => {
		}, (e) => console.log(e))
		localStorage.removeItem("token")
		nav("/login")
	}

	const icons = {
		"dark": <SunIcon />,
		"light": <MoonIcon />
	}

	if (token == null) {
		return (
			<HStack justifyContent={"space-between"} marginX={"1em"} marginTop={"0.5em"} marginBottom={"2.5em"}>
				<Text fontSize="xl" color="blue.300">GoChat</Text>
				<Button onClick={toggleColorMode}>{icons[colorMode]}</Button>
			</HStack>
		)
	}
	return (
		<HStack justifyContent={"space-between"} marginX={"1em"} marginTop={"0.5em"} marginBottom={"2.5em"}>
			<Text fontSize="xl" color="blue.300">GoChat</Text>
			<HStack gap={2}>
				<Text>{token.username}</Text>
				<Button onClick={handleLogout}>Logout</Button>
				<Button onClick={toggleColorMode}>{icons[colorMode]}</Button>
			</HStack>
		</HStack>
	)
}

export {HeaderComponent}