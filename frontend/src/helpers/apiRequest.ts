function request(url: string, options?: RequestInit, onSuccess?: (r: string) => void, onError?: (e: any) => void) {
	fetch(url, options).then(res => {
		res.json().then((v) => {
			if (res.status < 299) {
				if (onSuccess) onSuccess(JSON.stringify(v))
			} else {
				if (onError) onError(JSON.stringify(v))
			}
		})
	}, err => {
		if (onError) onError(JSON.stringify(err))
	})
}

export {request}