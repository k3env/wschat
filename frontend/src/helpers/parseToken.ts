import {JWT, UserInfo} from "@/types";
import {decodeToken} from "react-jwt";

export function parseToken(src: JWT): UserInfo {
	return {
		username: src.sub,
		expirity: new Date(src.exp*1000),
		session: src.sid
	}
}

export function getUserInfo(): UserInfo|null {
	const token = localStorage.getItem("token")
	if (token == null) {
		return null
	}
	const decoded = decodeToken<JWT>(token)
	if (decoded == null) {
		return null
	}
	return parseToken(decoded)
}