module github.com/k3env/wsdemo

go 1.22.1

require (
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/gorilla/websocket v1.5.1
	github.com/k3env/webcollection v0.0.0-20240319135815-717b3b9e6067
	golang.org/x/crypto v0.22.0
)

require (
	github.com/justinas/alice v1.2.0 // indirect
	github.com/urfave/negroni v1.0.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)
