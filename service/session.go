package service

import (
	"errors"
	"github.com/golang-jwt/jwt/v5"
	. "github.com/k3env/wsdemo/models"
	. "github.com/k3env/wsdemo/store"
	"net/http"
	"time"
)

type SessionService struct {
	store  Store[*Session]
	users  Store[*User]
	jwtKey []byte
}

func NewSessionService(store Store[*Session], userStore Store[*User], jwtKey []byte) *SessionService {
	return &SessionService{
		store:  store,
		users:  userStore,
		jwtKey: jwtKey,
	}
}

func (svc *SessionService) Login(username string, password string) (*http.Cookie, string, error) {
	user, ok := svc.users.Get(username)
	if !ok {
		return nil, "", errors.New("user not found")
	}
	if !user.VerifyPassword(password) {
		return nil, "", errors.New("wrong password")
	}
	sess := NewSession(user)
	cookie := svc.makeCookie(sess)
	access, err := svc.issueJWT(user, sess)
	if err != nil {
		return nil, "", err
	}
	err = svc.store.Put(sess.SessionId, sess)
	if err != nil {
		return nil, "", err
	}
	return cookie, access, nil
	//return sess, nil
	//return nil, errors.New("not implemented")
}

func (svc *SessionService) Logout(refreshToken string) error {
	sess, ok := svc.store.First(func(value *Session) bool {
		return value.RefreshToken == refreshToken
	})
	if !ok {
		return errors.New("session not found")
	}
	err := svc.store.Delete(sess.SessionId)
	if err != nil {
		return err
	}
	return errors.New("not implemented")
}

func (svc *SessionService) RenewTokens(refreshToken string) (*http.Cookie, string, error) {
	sess, ok := svc.store.First(func(value *Session) bool {
		return value.RefreshToken == refreshToken
	})
	if !ok {
		return nil, "", errors.New("session not found")
	}
	sess.GenerateRefreshToken()
	cookie := svc.makeCookie(sess)
	access, err := svc.issueJWT(sess.User, sess)
	if err != nil {
		return nil, "", err
	}
	return cookie, access, nil
	//return nil, "", errors.New("not implemented")
}

func (svc *SessionService) issueJWT(user *User, session *Session) (access string, err error) {
	payload := jwt.MapClaims{
		"sub": user.Username,
		"exp": time.Now().Add(time.Hour * 24).Unix(),
		"sid": session.SessionId,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, payload)
	access, err = token.SignedString(svc.jwtKey)
	return
}

func (svc *SessionService) makeCookie(session *Session) *http.Cookie {
	return &http.Cookie{
		Name:     "refresh_token",
		Value:    session.RefreshToken,
		HttpOnly: false,
		Secure:   false,
		SameSite: http.SameSiteLaxMode,
		MaxAge:   3600,
		Path:     "/",
	}
}
