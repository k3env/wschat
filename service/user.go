package service

import (
	"errors"
	. "github.com/k3env/wsdemo/models"
	. "github.com/k3env/wsdemo/store"
)

type UserService struct {
	store Store[*User]
}

func NewUserService(store Store[*User]) *UserService {
	return &UserService{store: store}
}

func (svc *UserService) Register(username string, password string) (*User, error) {
	if _, ok := svc.store.Get(username); ok {
		return nil, errors.New("user already exist")
	}
	u := NewUser(username, password)
	err := svc.store.Put(u.Username, u)
	if err != nil {
		return nil, err
	}
	return u, nil
}

//func (svc *UserService) Get(id string) (*User, error) {
//	user, ok := svc.store.Get(id)
//	if ok {
//		return user, nil
//	}
//	return nil, errors.New("user not found")
//}
