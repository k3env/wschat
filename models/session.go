package models

import (
	"fmt"
	"github.com/k3env/wsdemo/utils/randstr"
)

type Session struct {
	User         *User
	SessionId    string
	RefreshToken string
}

func NewSession(user *User) *Session {
	s := &Session{
		User:         user,
		SessionId:    "",
		RefreshToken: "",
	}
	s.generateSessionId()
	s.GenerateRefreshToken()
	return s
}

func (s *Session) GenerateRefreshToken() {
	str := randstr.NewGenerator().Make(32)
	s.RefreshToken = str
}

func (s *Session) generateSessionId() {
	str := randstr.NewGenerator().Make(8)
	s.SessionId = fmt.Sprintf("%s-%s", s.User.Username, str)
}
