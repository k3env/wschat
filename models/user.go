package models

import (
	"encoding/json"
	"golang.org/x/crypto/argon2"
)

var salt = []byte("StrongPasswordSeaSalt")

type User struct {
	Username string `json:"username"`
	Password string `json:"-"`
}

func (u *User) VerifyPassword(password string) bool {
	var hash []byte
	hash = argon2.Key([]byte(password), salt, 4, 64*1024, 2, 32)
	return u.Password == string(hash)
}

func (u *User) storePassword() {
	var hash []byte
	hash = argon2.Key([]byte(u.Password), salt, 4, 64*1024, 2, 32)
	u.Password = string(hash)
}

func (u *User) FromJson(data []byte) error {
	err := json.Unmarshal(data, u)
	u.storePassword()
	return err
}

func (u *User) ToJson() ([]byte, error) {
	return json.Marshal(u)
}

func NewUser(username string, password string) *User {
	u := &User{
		Username: username,
		Password: password,
	}
	u.storePassword()
	return u
}
