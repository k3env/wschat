package store

type FindPredicate[T interface{}] func(value T) bool

type Store[T interface{}] interface {
	Get(key string) (T, bool)
	Put(key string, value T) error
	Delete(key string) error
	First(callback FindPredicate[T]) (T, bool)
	Many(callback FindPredicate[T]) []T
}
