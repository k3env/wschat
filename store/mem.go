package store

type MemStore[T interface{}] struct {
	store map[string]T
}

func NewMemStorage[T interface{}]() *MemStore[T] {
	return &MemStore[T]{
		store: make(map[string]T),
	}
}

func (s *MemStore[T]) Get(key string) (T, bool) {
	v, ok := s.store[key]
	return v, ok
}

func (s *MemStore[T]) Put(key string, value T) error {
	s.store[key] = value
	return nil
}

func (s *MemStore[T]) Delete(key string) error {
	if _, ok := s.store[key]; ok {
		delete(s.store, key)
	}
	return nil
}

func (s *MemStore[T]) First(callback FindPredicate[T]) (T, bool) {
	var v T

	for _, value := range s.store {
		if callback(value) {
			return value, true
		}
	}

	return v, false
}

func (s *MemStore[T]) Many(callback FindPredicate[T]) []T {
	var vs = make([]T, 0)
	for _, value := range s.store {
		if callback(value) {
			vs = append(vs, value)
		}
	}
	return vs
}
