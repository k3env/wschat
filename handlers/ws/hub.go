package ws

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

type eventType uint8

const (
	eventDisconnect eventType = iota
	eventConnect
)

type event struct {
	client *Client
	event  eventType
}

type message struct {
	Text      string `json:"text"`
	From      string `json:"from"`
	To        string `json:"to"`
	Timestamp uint64 `json:"timestamp"`
}

type Hub struct {
	clients  map[string]*Client
	upgrader websocket.Upgrader
	events   chan *event
	msg      chan *message
}

func NewHub() *Hub {
	return &Hub{
		upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
		clients: map[string]*Client{},
		events:  make(chan *event),
		msg:     make(chan *message),
	}
}

func (hub *Hub) Run() {
	for {
		select {
		case msg := <-hub.msg:
			client, ok := hub.clients[msg.To]
			if !ok {
				continue
			}
			msgData, _ := json.Marshal(msg)
			client.send <- msgData
		case e := <-hub.events:
			switch e.event {
			case eventDisconnect:
				if _, ok := hub.clients[e.client.id]; ok {
					delete(hub.clients, e.client.id)
					e.client.conn.Close()
				}
			case eventConnect:
				if _, ok := hub.clients[e.client.id]; !ok {
					hub.clients[e.client.id] = e.client
					go e.client.Run(hub)
				}
			}
		}
	}
}

func (hub *Hub) ClientConnect(client *Client) {

}

func (hub *Hub) routeMsg(msg *message) {
	receiver := msg.To
	for _, client := range hub.clients {
		if client.id == receiver {
			data, _ := json.Marshal(msg)
			client.send <- data
		}
	}
}

func (hub *Hub) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	id := r.PathValue("id")
	if id == "" {
		log.Println("id not supplied")
		return
	}
	conn, err := hub.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := NewClient(conn, id)
	hub.ClientConnect(client)
}
