package api

import (
	"encoding/json"
	"github.com/k3env/wsdemo/service"
	"io"
	"net/http"
)

type UserHandler struct {
	users    *service.UserService
	sessions *service.SessionService
}

func NewUserHandler(userSvc *service.UserService, sessionSvc *service.SessionService) (h *UserHandler) {
	h = &UserHandler{
		users:    userSvc,
		sessions: sessionSvc,
	}
	return
}

//func (h *UserHandler) generateRefreshToken() string {
//	return h.gen.Make(32)
//}
//
//func (h *UserHandler) generateSessionID() string {
//	return h.gen.Make(4)
//}

//func (h *UserHandler) issueTokens(user *User, sessId string) (access string, refresh string, err error) {
//	payload := jwt.MapClaims{
//		"sub": user.Username,
//		"exp": time.Now().Add(time.Hour * 24).Unix(),
//		"sid": sessId,
//	}
//	token := jwt.NewWithClaims(jwt.SigningMethodHS512, payload)
//	refresh = h.generateRefreshToken()
//	access, err = token.SignedString(jwtSecretKey)
//	return
//}

func (h *UserHandler) HandleRegister(w http.ResponseWriter, r *http.Request) {

	je := json.NewEncoder(w)
	u := make(map[string]string)
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	err = json.Unmarshal(body, &u)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	username, ok := u["username"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": "missing username"})
		return
	}
	password, ok := u["password"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": "missing password"})
		return
	}
	//u := &User{}
	//body, err = io.ReadAll(r.Body)

	user, err := h.users.Register(username, password)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	//err = json.Unmarshal(body, &u)
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": err.Error()})
	//	return
	//}
	//_, ok := h.users.Get(u.Username)
	//if ok {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": "User already registered"})
	//	return
	//}
	//err = u.StorePassword()
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": err.Error()})
	//	return
	//}
	//h.users.Put(u.Username, u)
	_ = je.Encode(map[string]any{"status": "ok", "message": "User registered", "data": user})
}

func (h *UserHandler) HandleLogin(w http.ResponseWriter, r *http.Request) {
	var err error
	var body []byte
	je := json.NewEncoder(w)
	u := make(map[string]string)
	body, err = io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	err = json.Unmarshal(body, &u)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	username, ok := u["username"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": "missing username"})
		return
	}
	password, ok := u["password"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": "missing password"})
		return
	}
	cookie, access, err := h.sessions.Login(username, password)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	http.SetCookie(w, cookie)
	//user, err := h.users.Get(u.Username)
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": err.Error()})
	//	return
	//}
	//if !user.VerifyPassword(u.Password) {
	//	w.WriteHeader(http.StatusForbidden)
	//	je.Encode(map[string]string{"status": "error", "error": "Incorrect password"})
	//	return
	//}
	//sessId := user.Username + "-" + h.generateSessionID()
	//access, refresh, err := h.issueTokens(user, sessId)
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": err.Error()})
	//	return
	//}
	//
	//cookie := http.Cookie{
	//	Name:     "refreshToken",
	//	Value:    refresh,
	//	HttpOnly: false,
	//	Secure:   false,
	//	SameSite: http.SameSiteLaxMode,
	//	MaxAge:   3600,
	//	Path:     "/",
	//}
	//http.SetCookie(w, &cookie)
	//h.sessions[sessId] = &Session{
	//	User:         user,
	//	SessionId:    sessId,
	//	RefreshToken: refresh,
	//}
	_ = je.Encode(map[string]interface{}{"status": "ok", "data": access})
}

func (h *UserHandler) HandleTokenRenew(w http.ResponseWriter, r *http.Request) {
	je := json.NewEncoder(w)
	oldCookie, err := r.Cookie("refreshToken")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	cookie, access, err := h.sessions.RenewTokens(oldCookie.Value)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}

	//var s *Session
	//var id string
	//var ok = false
	//
	//for key, sess := range h.sessions {
	//	if sess.RefreshToken == oldCookie.Value {
	//		s, id, ok = sess, key, true
	//		return
	//	}
	//}
	//
	//if !ok {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": "session not found"})
	//	return
	//}
	//access, refresh, err := h.issueTokens(s.User, id)
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": err.Error()})
	//	return
	//}
	//s.RefreshToken = refresh
	//h.sessions[id] = s
	////delete(h.sessions, oldCookie.Value)
	//
	//cookie := http.Cookie{
	//	Name:     "refreshToken",
	//	Value:    refresh,
	//	HttpOnly: false,
	//	Secure:   false,
	//	SameSite: http.SameSiteLaxMode,
	//	MaxAge:   3600,
	//	Path:     "/",
	//}
	http.SetCookie(w, cookie)
	_ = je.Encode(map[string]string{"status": "ok", "data": access})
}

func (h *UserHandler) HandleLogout(w http.ResponseWriter, r *http.Request) {
	je := json.NewEncoder(w)
	cookie, err := r.Cookie("refreshToken")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	err = h.sessions.Logout(cookie.Value)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_ = je.Encode(map[string]string{"status": "error", "error": err.Error()})
		return
	}
	//
	//var id string
	//var ok = false
	//
	//for key, sess := range h.sessions {
	//	if sess.RefreshToken == cookie.Value {
	//		id, ok = key, true
	//		return
	//	}
	//}
	//if !ok {
	//	w.WriteHeader(http.StatusBadRequest)
	//	je.Encode(map[string]string{"status": "error", "error": "session not found"})
	//	return
	//}
	cookie.MaxAge = -1
	http.SetCookie(w, cookie)
	//delete(h.sessions, id)
	_ = je.Encode(map[string]string{"status": "ok", "data": "session terminated"})
}
