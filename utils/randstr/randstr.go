package randstr

import (
	"math/rand"
	"time"
)

type Generator struct {
	letters []rune
	rng     *rand.Rand
}

func NewGenerator() *Generator {
	rng := rand.New(rand.NewSource(time.Now().Unix()))
	return &Generator{letters: []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), rng: rng}
}

func (g *Generator) Make(length int) string {
	b := make([]rune, length)
	for i := range b {
		pos := g.rng.Intn(len(g.letters))
		b[i] = g.letters[pos]
	}
	return string(b)
}
