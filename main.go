package main

import (
	"embed"
	"github.com/justinas/alice"
	spapkg "github.com/k3env/webcollection/handlers/spa"
	"github.com/k3env/webcollection/middlewares"
	"github.com/k3env/wsdemo/handlers/api"
	mws "github.com/k3env/wsdemo/handlers/middlewares"
	"github.com/k3env/wsdemo/handlers/ws"
	. "github.com/k3env/wsdemo/models"
	"github.com/k3env/wsdemo/service"
	"github.com/k3env/wsdemo/store"
	"log"
	"net/http"
)

//go:embed web
var efs embed.FS

func main() {
	userStore := store.NewMemStorage[*User]()
	sessStore := store.NewMemStorage[*Session]()
	userSvc := service.NewUserService(userStore)
	sessSvc := service.NewSessionService(sessStore, userStore, []byte("secretJwtKey"))
	usrHandler := api.NewUserHandler(userSvc, sessSvc)
	fsh := spapkg.NewEmbedFS(&efs)
	spa := spapkg.NewSPA("web", "index.html", fsh)

	cors := func(next http.Handler) http.Handler {
		return middlewares.Cors(next, middlewares.OriginAll)
	}
	defChain := alice.New(middlewares.Log, cors)
	apiChain := defChain.Append(mws.JsonMiddleware)

	hub := ws.NewHub()
	go hub.Run()

	r := http.NewServeMux()
	r.Handle("GET /ws/{id}", hub)

	r.Handle("POST /api/login", apiChain.ThenFunc(usrHandler.HandleLogin))
	r.Handle("POST /api/register", apiChain.ThenFunc(usrHandler.HandleRegister))
	r.Handle("POST /api/renew", apiChain.ThenFunc(usrHandler.HandleTokenRenew))
	r.Handle("POST /api/logout", apiChain.ThenFunc(usrHandler.HandleLogout))

	r.Handle("/", defChain.Then(spa))
	//hub = newHub()
	//go hub.run()

	err := http.ListenAndServe(":8000", r)
	if err != nil {
		log.Fatal(err)
	}
}
